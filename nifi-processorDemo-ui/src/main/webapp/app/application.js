/* global Slick, nf */

$(document).ready(function () {
    ua.editable = $('#attribute-updater-editable').text() === 'true';
    ua.init();
});

var ua = {
    editable: false,
    
    /**
     * Initializes this web application.
     * 
     * @returns {undefined}
     */
    init: function () {
        var evaluationContext = $.ajax({
            type: 'GET',
            url: 'api/standard/processor/details?' + $.param({
                processorId: ua.getProcessorId()
            })
        }).done(function (ret) {
            $('#processor_name').prepend(ret['name']);
            $('#int_param').val(ret['properties']['int_test']);
        });

        $('#changeNameButton').on('click', function () {
            var urlParams = 'processorId='+ua.getProcessorId()+'&revisionId='+ua.getRevision()+'&clientId='+ua.getClientId();
            var data = {};
            data['processorName'] = $('#processor_name').val();
            $.ajax({
                type: 'PUT',
                url: 'api/standard/processor/changeName?'+urlParams,
                data: JSON.stringify(data),
                processData: false,
                contentType: 'application/json'
            }).then(function (response) {

            }, function (xhr, status, error) {
                // show an error message
            });
        });
        $('#updateButton').on('click', function () {
            var int_param=$('#int_param').val()
            var urlParams = 'processorId='+ua.getProcessorId()+'&revisionId='+ua.getRevision()+'&clientId='+ua.getClientId();
            $.ajax({
                type: 'PUT',
                url: 'api/standard/processor/properties?'+urlParams,
                data: JSON.stringify({"int_test":int_param}),
                processData: false,
                contentType: 'application/json'
            }).then(function (response) {
                $('#retValue').text("修改成功");
            }, function (xhr, status, error) {
                // show an error message
            });
        });
        $('#transformButton').on('click', function () {
            var sourceDate=$('#sourceDate').val()
            $.ajax({
                type: 'POST',
                url: 'api/standard/datetransform/execute',
                data: JSON.stringify({"sourceDate":sourceDate}),
                processData: false,
                contentType: 'application/json'
            }).then(function (response) {
                $('#standardDate').text(response.standardDate);
            }, function (xhr, status, error) {
                // show an error message
            });
        });
    },

    /**
     * Gets the client id.
     * 
     * @returns 
     */
    getClientId: function () {
        return $('#attribute-updater-client-id').text();
    },
    
    /**
     * Gets the revision.
     * 
     * @returns 
     */
    getRevision: function () {
        return $('#attribute-updater-revision').text();
    },
    
    /**
     * Gets the processor id.
     * 
     * @returns
     */
    getProcessorId: function () {
        return $('#attribute-updater-processor-id').text();
    }
};