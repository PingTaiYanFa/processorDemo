<%--
 Licensed to the Apache Software Foundation (ASF) under one or more
  contributor license agreements.  See the NOTICE file distributed with
  this work for additional information regarding copyright ownership.
  The ASF licenses this file to You under the Apache License, Version 2.0
  (the "License"); you may not use this file except in compliance with
  the License.  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" session="false" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="../nifi/assets/jquery-ui-dist/jquery-ui.min.css" type="text/css" />
        <link rel="stylesheet" href="../nifi/assets/slickgrid/slick.grid.css" type="text/css" />
        <link rel="stylesheet" href="../nifi/css/slick-nifi-theme.css" type="text/css" />
        <link rel="stylesheet" href="../nifi/js/jquery/modal/jquery.modal.css" type="text/css" />
        <link rel="stylesheet" href="../nifi/js/jquery/combo/jquery.combo.css" type="text/css" />
        <link rel="stylesheet" href="../nifi/assets/qtip2/dist/jquery.qtip.min.css" type="text/css" />
        <link rel="stylesheet" href="../nifi/js/codemirror/lib/codemirror.css" type="text/css" />
        <link rel="stylesheet" href="../nifi/js/codemirror/addon/hint/show-hint.css" type="text/css" />
        <link rel="stylesheet" href="../nifi/js/jquery/nfeditor/jquery.nfeditor.css" type="text/css" />
        <link rel="stylesheet" href="../nifi/js/jquery/nfeditor/languages/nfel.css" type="text/css" />
        <link rel="stylesheet" href="../nifi/fonts/flowfont/flowfont.css" type="text/css" />
        <link rel="stylesheet" href="../nifi/assets/font-awesome/css/font-awesome.min.css" type="text/css" />
        <link rel="stylesheet" href="../nifi/assets/reset.css/reset.css" type="text/css" />
        <link rel="stylesheet" href="css/main.css" type="text/css" />
        <link rel="stylesheet" href="../nifi/css/common-ui.css" type="text/css" />
        <script type="text/javascript" src="../nifi/assets/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" src="../nifi/js/jquery/jquery.center.js"></script>
        <script type="text/javascript" src="../nifi/js/jquery/jquery.each.js"></script>
        <script type="text/javascript" src="../nifi/js/jquery/jquery.tab.js"></script>
        <script type="text/javascript" src="../nifi/js/jquery/modal/jquery.modal.js"></script>
        <script type="text/javascript" src="../nifi/js/jquery/combo/jquery.combo.js"></script>
        <script type="text/javascript" src="../nifi/js/jquery/jquery.ellipsis.js"></script>
        <script type="text/javascript" src="../nifi/assets/jquery-ui-dist/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../nifi/assets/qtip2/dist/jquery.qtip.min.js"></script>
        <script type="text/javascript" src="../nifi/assets/JSON2/json2.js"></script>
        <script type="text/javascript" src="../nifi/assets/slickgrid/lib/jquery.event.drag-2.3.0.js"></script>
        <script type="text/javascript" src="../nifi/assets/slickgrid/plugins/slick.cellrangedecorator.js"></script>
        <script type="text/javascript" src="../nifi/assets/slickgrid/plugins/slick.cellrangeselector.js"></script>
        <script type="text/javascript" src="../nifi/assets/slickgrid/plugins/slick.cellselectionmodel.js"></script>
        <script type="text/javascript" src="../nifi/assets/slickgrid/plugins/slick.rowselectionmodel.js"></script>
        <script type="text/javascript" src="../nifi/assets/slickgrid/slick.formatters.js"></script>
        <script type="text/javascript" src="../nifi/assets/slickgrid/slick.editors.js"></script>
        <script type="text/javascript" src="../nifi/assets/slickgrid/slick.dataview.js"></script>
        <script type="text/javascript" src="../nifi/assets/slickgrid/slick.core.js"></script>
        <script type="text/javascript" src="../nifi/assets/slickgrid/slick.grid.js"></script>
        <script type="text/javascript" src="../nifi/js/codemirror/lib/codemirror-compressed.js"></script>
        <script type="text/javascript" src="../nifi/js/nf/nf-namespace.js"></script>
        <script type="text/javascript" src="../nifi/js/nf/nf-storage.js"></script>
        <script type="text/javascript" src="../nifi/js/nf/nf-ajax-setup.js"></script>
        <script type="text/javascript" src="../nifi/js/nf/nf-universal-capture.js"></script>
        <script type="text/javascript" src="../nifi/js/jquery/nfeditor/languages/nfel.js"></script>
        <script type="text/javascript" src="../nifi/js/jquery/nfeditor/jquery.nfeditor.js"></script>
        <script type="text/javascript" src="app/application.js"></script>
        <title>Update Attribute</title>
    </head>
    <body>
        <div id="attribute-updater-processor-id" class="hidden"><%= request.getParameter("id") == null ? "" : org.apache.nifi.util.EscapeUtils.escapeHtml(request.getParameter("id")) %></div>
        <div id="attribute-updater-client-id" class="hidden"><%= request.getParameter("clientId") == null ? "" : org.apache.nifi.util.EscapeUtils.escapeHtml(request.getParameter("clientId")) %></div>
        <div id="attribute-updater-revision" class="hidden"><%= request.getParameter("revision") == null ? "" : org.apache.nifi.util.EscapeUtils.escapeHtml(request.getParameter("revision")) %></div>
        <div id="attribute-updater-editable" class="hidden"><%= request.getParameter("editable") == null ? "" : org.apache.nifi.util.EscapeUtils.escapeHtml(request.getParameter("editable")) %></div>
        <div id="update-properties-content">
            <div id="update-properties-panel">
                <div id="update-properties-container">
                    <div id="update-properties-label" class="large-label">修改任务名称</div>
                </div>
                <div id="properties-container">
                    <input type="text" id="processor_name" size="20"/>
                    <input type="button" style="width:90px;height:30px;" value="修改" id="changeNameButton" />
                </div>
                <div id="update-properties-container2">
                    <div id="update-properties-label2" class="large-label">修改int参数值</div>
                </div>
                <div id="properties-container2">
                    <input type="text" id="int_param" size="20"/>
                    <input type="button" style="width:90px;height:30px;" value="修改" id="updateButton" />
                    <span id="retValue"></span>
                </div>
            </div>
        </div>
        <div id="singletest-content">
            <div id="single-container">
                <div id="test-label" class="large-label">待修正时间</div>
            </div>
            <div id="date-transform-container">
                <input type="text" id="sourceDate" value="20180601111111" size="20"/>
                <input type="button" style="width:90px;height:30px;" value="修正" id="transformButton" />
                <span id="standardDate"></span>
            </div>
        </div>
    </body>
</html>