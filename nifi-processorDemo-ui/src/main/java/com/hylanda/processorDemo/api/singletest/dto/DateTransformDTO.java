package com.hylanda.processorDemo.api.singletest.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by tujl on 2018/5/25.
 */
@XmlRootElement
public class DateTransformDTO {
    private String sourceDate;

    public String getSourceDate() {
        return sourceDate;
    }

    public void setSourceDate(String sourceDate) {
        this.sourceDate = sourceDate;
    }
}
