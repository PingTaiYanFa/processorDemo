/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hylanda.processorDemo.api.singletest;

import com.alibaba.fastjson.JSONObject;
import com.hylanda.processorDemo.api.AbstractStandardResource;
import com.hylanda.processorDemo.api.singletest.dto.DateTransformDTO;
import com.hylanda.processors.processorDemo.service.MyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/standard/datetransform")
public class TransformDateResource extends AbstractStandardResource {

    private static final Logger logger = LoggerFactory.getLogger(TransformDateResource.class);
    private final static String DEFAULT_CHARSET = "UTF-8";

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/execute")
    public Response executeSpec(DateTransformDTO specificationDTO) {
        try {
            MyService myService = new MyService();
            String date = myService.formatDate(specificationDTO.getSourceDate());
            JSONObject jb = new JSONObject();
            jb.put("standardDate", date);
            return Response.ok(jb.toJSONString()).build();
        }catch(final Exception e){
            logger.error("Execute Failed - " + e.toString());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
