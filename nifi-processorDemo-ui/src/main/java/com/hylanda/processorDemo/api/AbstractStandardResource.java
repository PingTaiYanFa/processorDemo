package com.hylanda.processorDemo.api;

import org.apache.nifi.web.NiFiWebConfigurationContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

public abstract class AbstractStandardResource {

    @Context
    protected ServletContext servletContext;

    @Context
    protected HttpServletRequest request;


    protected NiFiWebConfigurationContext getWebConfigurationContext(){
        return (NiFiWebConfigurationContext) servletContext.getAttribute("nifi-web-configuration-context");
    }
}