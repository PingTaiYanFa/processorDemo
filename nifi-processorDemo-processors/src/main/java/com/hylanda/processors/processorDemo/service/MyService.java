package com.hylanda.processors.processorDemo.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by tujl on 2018/5/29.
 */
public class MyService {
    public String formatDate(String sdate) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMddhhmmss");
        return sdf.format(sdf2.parse(sdate));
    }
}
