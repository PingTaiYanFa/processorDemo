/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hylanda.processors.processorDemo;

import org.apache.nifi.annotation.behavior.ReadsAttribute;
import org.apache.nifi.annotation.behavior.ReadsAttributes;
import org.apache.nifi.annotation.behavior.WritesAttribute;
import org.apache.nifi.annotation.behavior.WritesAttributes;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.SeeAlso;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.components.*;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.*;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.util.StandardValidators;

import java.util.*;

@Tags({"MyProcessor"})
@CapabilityDescription("This is the description of MyProcessor")
@SeeAlso({})
@ReadsAttributes({@ReadsAttribute(attribute="", description="")})
@WritesAttributes({@WritesAttribute(attribute="", description="")})
public class MyProcessor extends AbstractProcessor {
    public static final Validator SELF_VALIDATOR = new Validator() {
        @Override
        public ValidationResult validate(final String subject, final String value, final ValidationContext context) {
            String reason = null;
            try {
                if(!"123".equals(value)){
                    reason = "参数必须是123";
                }
            } catch (final RuntimeException e) {
                reason = e.getClass().getName();
            }

            return new ValidationResult.Builder()
                    .subject(subject)
                    .input(value)
                    .explanation(reason)
                    .valid(reason == null)
                    .build();
        }
    };
    public static final PropertyDescriptor CHECKBOX_TEST = new PropertyDescriptor
            .Builder().name("checkbox_test")
            .displayName("checkbox_test")
            .description("checkbox_test")
            .required(true)
            .multiValues(true)
            .allowableValues(new AllowableValue("0","资讯"),new AllowableValue("4","微博"))
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();
    public static final PropertyDescriptor SELECT_TEST = new PropertyDescriptor
            .Builder().name("select_test")
            .displayName("select_test")
            .description("select_test")
            .allowableValues(new AllowableValue("1","关键词"),new AllowableValue("2","判定图"))
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();
    public static final PropertyDescriptor INT_TEST = new PropertyDescriptor
            .Builder().name("int_test")
            .displayName("int_test")
            .required(true)
            .addValidator(StandardValidators.INTEGER_VALIDATOR)
            .build();
    public static final PropertyDescriptor SELF_TEST = new PropertyDescriptor
            .Builder().name("self_test")
            .displayName("self_test")
            .required(true)
            .addValidator(SELF_VALIDATOR)
            .build();
    public static final Relationship MY_RELATIONSHIP = new Relationship.Builder()
            .name("success")
            .description("成功")
            .build();

    private List<PropertyDescriptor> descriptors;

    private Set<Relationship> relationships;

    @Override
    protected void init(final ProcessorInitializationContext context) {
        final List<PropertyDescriptor> descriptors = new ArrayList<PropertyDescriptor>();
        descriptors.add(CHECKBOX_TEST);
        descriptors.add(SELECT_TEST);
        descriptors.add(INT_TEST);
        descriptors.add(SELF_TEST);
        this.descriptors = Collections.unmodifiableList(descriptors);

        final Set<Relationship> relationships = new HashSet<Relationship>();
        relationships.add(MY_RELATIONSHIP);
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    public Set<Relationship> getRelationships() {
        return this.relationships;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return descriptors;
    }

    @OnScheduled
    public void onScheduled(final ProcessContext context) {
        String selectValue = context.getProperty(SELECT_TEST).getValue();
        getLogger().info(selectValue);
        String checkValue = context.getProperty(CHECKBOX_TEST).getValue();
        getLogger().info(checkValue);
    }

    @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
        FlowFile flowFile = session.get();
        if ( flowFile == null ) {
            return;
        }
    }
}
